package DataStructures;

/**
 *
 * @author diazj
 */
public class NotFoundKey {
    private String message;
    
    public NotFoundKey(String key) {
        message = "The key " + key + " was not present in the searched JSON.";
    }
    
    public String getMessage() {
        return message;
    }
}

package DataStructures;

import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author diazj
 */
public class JSONObject {
    private boolean debug = false;
    private Map jsonMap;
    private Map<String, Object> dataMap = new HashMap();
    
    /**
     * Default empty constructor. Search functions can be used to search data
     * on any provided JSON.
     */
    public JSONObject() {
        
    }
    
    /**
     * Initializes this object with the provided JSON string.
     * @param json 
     */
    public JSONObject(String json) {
        jsonMap = new Gson().fromJson(json, Map.class);
    }
    
    /**
     * Sets the debug mode on or off.
     * 
     * @param flag True will activate the debug mode, showing various messages
     * and information about the various operations at the cost of performance;
     * however, the performance impact should be minimal.
     */
    public void debug(boolean flag) {
        debug = flag;
    }
    
    /**
     * Explores the provided JSON once and stores every key (including it's path
     * resolution) in a hash map for much quicker access at the cost of process 
     * time and memory since the JSON must be explored in it's entirety.
     * 
     * @param json JSON Map to be explored.
     * @param searchLevel Indicates how many secondary JSON Objects 
     * have been accessed. Used for debug purposes only; otherwise irrelevant.
     * @return Object for each value.
     */
    public Object exploreJSON(Map json, String searchLevel) {
        if (debug) {
            System.out.println("Starting search on level: " + searchLevel + ".");
        }
        
        Set<String> keySet = json.keySet();
        for (String key : keySet) {
            if (debug) {
                System.out.println("Analazing key-value pair " + key);
            }
            
            String nextLevel = "";
            if (searchLevel.isEmpty()) {
                nextLevel = key;
            }
            else {
                nextLevel += searchLevel + "." + key;
            }
            
            try{
                Map subjson = subjson = (Map) json.get(key);
                dataMap.put(nextLevel, exploreJSON(subjson, nextLevel));
            } catch (ClassCastException ex) {
                if (debug) {
                    System.out.println("Key " + key + " found. Value: " + json);
                }
                dataMap.put(nextLevel, json.get(key));
            }
        }
        
        if (debug) {
            System.out.println("Search on level " + searchLevel + " has been finished.");
        }
        return json;
    }
    
    /**
     * Searches through a map object for the value stored at the specified key.
     * A key can be a single word or multiple words separated by a dot.
     * The provided map is explored recursively until the key is found
     * or it's determined that it's not present in the specified key path.
     * 
     * @param key Represents the name of a JSON property.
     * @param json Map Object representing a JSON.
     * @return null or an object that represents the data stored at the
     * specified key.
     */
    public Object search(String key, Map json) {
        String[] searchKeys = key.split("\\.");
        
        if (json.containsKey(searchKeys[0])) {
            if (debug) {
                System.out.println("Key found: " + searchKeys[0]);
            }
        } else {
            if (debug) {
                System.out.println("Key is not present: " + key);
            }
            return new NotFoundKey(key);
        }
        
        Object value;
        int searchLevels = searchKeys.length;
        if (searchLevels > 1) {
            try {
                Map subjson = (Map) json.get(searchKeys[0]);
                
                String newKey = "";
                for (int levels = 1; levels < searchLevels; levels++) {
                    newKey += searchKeys[levels];
                    if (levels < searchLevels - 1) {
                        newKey += ".";
                    }
                }
                
                value = this.search(newKey, subjson);
                if (value != null) {
                    return value;
                }
            } catch (Exception ex) {
                if (debug) {
                    System.out.println("Data structure is not a JSON. Error on key: " + searchKeys[0]);
                }
                return null;
            }
        }
        
        return json.get(searchKeys[0]);
    }
    
    /**
     * The provided map is explored recursively until the first match 
     * of the key is found. 
     * 
     * Returns null after searching for the provided key in the entire 
     * JSON object without success and as such, worst case performance 
     * can be very low.
     * 
     * @param key Object used to match a Map key.
     * @param json Object representing a JSON. The object must be parse-able to
     * Map.
     * @return null or an object that represents the data stored at the
     * specified key.
     */
    public Object searchDeep(Object key, Object json) {
        if (debug) {
            System.out.println("Searching for key: " + key + ".");
        }
        
        Map jsonMap = null;
        
        try {
            jsonMap = (Map) json;
        } catch (ClassCastException ex) {
            if (debug) {
                System.out.println("Data structure is not a JSON. Error on key: " + key);
            }
            return null;
        }
        
        Set<Object> keySet = jsonMap.keySet();
        Object value;
        
        if(keySet.contains(key)) {
            System.out.println("Found");
            value = jsonMap.get(key);
            System.out.println(value);
            return value;
        } else {
            Object[] keys = keySet.toArray();
            
            for (Integer index = 0; index < keys.length; index++) {
                value = this.searchDeep(key, jsonMap.get(keys[index]));
                if (value != null) {
                    if (debug) {
                        System.out.println("Key value found. Key: " + key + ", value: " + value);
                    }
                    return value;
                }
            }
        }
        
        if (debug) {
            System.out.println("The key: " + key + " could not be found.");
        }
        return null;
    }
    
    /**
     * Get the JSON map object created off of the constructor's string input.
     * 
     * @return JSON map object. 
     */
    public Map getJson() {
        if (debug) {
            Set<String> keySet = jsonMap.keySet();
            for (Object key: keySet) {
                System.out.println("Key: " + key + ", value: " + jsonMap.get(key));
            }
        }
        
        return jsonMap;
    }
    
    /**
     * Get a preprocessed hash map containing all of the JSON's data.
     * exploreJSON method must be called first, otherwise the hash map
     * will be empty.
     * 
     * @return Map object containing the result of the exploreJSON method.
     */
    public Map getParameters() {
        if (debug) {
            Set<String> keySet = dataMap.keySet();
            for (Object key: keySet) {
                System.out.println("Key: " + key + ", value: " + dataMap.get(key));
            }
        }
        
        return dataMap;
    }
}

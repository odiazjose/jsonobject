/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataStructures;

import java.util.Map;

/**
 *
 * @author diazj
 */
public class Main {
    public static void main(String args[]) {
        JSONObject p = new JSONObject("{'classname': 'Users', 'method': 'register', "
                + "parameters: {'name': 'Jose', 'lastName': ['Ortega', 'Diaz'], "
                + "'emails': {'gmail': 'odiaz.jose@gmail.com', "
                + "'hotmail': 'diazjosekiyo@hotmail.com', "
                + "'others': {'yahoo': 'random@yahoo.com'}}}}");
        
        p.exploreJSON(p.getJson(), "");
        Map jsonData = p.getParameters();
        System.out.println(jsonData.get("parameters.emails"));
    }
}
